package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    double getBookPrice(String isbn) {
        HashMap<String, Double> nums = new HashMap<String, Double>();
        nums.put("1",10.0);
        nums.put("2",45.0);
        nums.put("3",20.0);
        nums.put("4",35.0);
        nums.put("5",50.0);
        return nums.getOrDefault(isbn, 0.0);
    }
}
